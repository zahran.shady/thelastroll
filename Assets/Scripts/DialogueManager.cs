﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public DialogueManager instance;
    public DialogueElement currentDialogue;
    public Image sceneImage;
    public TextMeshProUGUI textArea;
    public TextMeshProUGUI speakerName;
    public GameObject buttonScrollDown, buttonScrollUp;
    private int currentSentenceIndex;
    private bool isTyping = false;
    public float transitionTime = 1.0f;
    public Menu mainContainer, emptyContainer;
    public void InitDialogue()
    {
        //Debug.Log("called init dialogue");
        HideScrollButtons();
        textArea.text = "";
        speakerName.text = "";
        currentSentenceIndex = 0;
        sceneImage.sprite = currentDialogue.image;

    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        currentSentenceIndex = 0;
    }

    private void Start()
    {
        InitDialogue();
    }

    public void ResetValues()
    {
        HideScrollButtons();
        textArea.text = "";
        speakerName.text = "";
        currentSentenceIndex = 0;
    }

    void HideScrollButtons()
    {
        buttonScrollDown.SetActive(false);
        buttonScrollUp.SetActive(false);
    }

    void LoadScrollButtons()
    {
        if (currentSentenceIndex < currentDialogue.sentences.Length - 1)
        {
            buttonScrollDown.SetActive(true);
        }
        else
        {
            buttonScrollDown.SetActive(false);
        }
        if (currentSentenceIndex > 0)
        {
            buttonScrollUp.SetActive(true);
        }
        else
        {
            buttonScrollUp.SetActive(false);
        }
    }

    public void NextSentence()
    {
        HideScrollButtons();
        if (currentSentenceIndex == currentDialogue.sentences.Length - 1)
        {

        }
        currentSentenceIndex++;
        StartCoroutine(LoadText(currentDialogue.sentences[currentSentenceIndex]));
    }

    public void PreviousSentence()
    {
        HideScrollButtons();
        currentSentenceIndex--;
        StartCoroutine(LoadText(currentDialogue.sentences[currentSentenceIndex]));
    }

    IEnumerator LoadText(string sentence)
    {
        isTyping = true;
        textArea.text = "";
        speakerName.text = currentDialogue.characterName;
        foreach (var letter in sentence.ToCharArray())
        {
            textArea.text += letter;
            yield return null;
        }
        LoadScrollButtons();
        isTyping = false;
    }

    public void StartDialogue()
    {
        PlayDialogueAudio();
        StartCoroutine(LoadText(currentDialogue.sentences[currentSentenceIndex]));
    }

    public void NextDialogue()
    {
        if (currentDialogue.nextDialogue != null)
        {
            currentDialogue = currentDialogue.nextDialogue;
            StartTransition();
        }
    }

    public void PreviousDialog()
    {
        if (currentDialogue.previousDialogue != null)
        {
            currentDialogue = currentDialogue.previousDialogue;
            StartTransition();

        }
    }

    public void StartTransition()
    {
        HideScrollButtons();
        ZUIManager.Instance.OpenMenu(emptyContainer);
        StartCoroutine(Fade());
        
    }

    IEnumerator Fade()
    {
        yield return new WaitForSeconds(transitionTime);
        ZUIManager.Instance.OpenMenu(mainContainer);
    }

    void PlayDialogueAudio()
    {
        if (currentDialogue.audioName!="")
        {
            SoundManager.instance.Play(currentDialogue.audioName);
        }
        else
        {
            Debug.LogWarning("no audio was specified for " + currentDialogue.name);
        }
        
    }
}
