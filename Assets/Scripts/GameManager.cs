﻿using Fungus;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Flowchart GameFlowchart;
    public string StartingBlock;
    public TMP_InputField PlayerName;
    string playerPronounSubject, playerPronounObject;
    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        if (string.IsNullOrWhiteSpace(PlayerName.text))
        {
            GameFlowchart.SetStringVariable("PlayerName", "Anonymous");
        }
        else
        {
            GameFlowchart.SetStringVariable("PlayerName", PlayerName.text);
        }

        //GameFlowchart.SetStringVariable("PlayerPronounsSubject", playerPronounSubject);
        //GameFlowchart.SetStringVariable("PlayerPronounsObject", playerPronounObject);
        MainMenu.instance.NameSelectionUI.SetActive(false);
        GameFlowchart.ExecuteBlock(StartingBlock);
    }

    public void SetPronouns(string pronouns)
    {
        string[] pronounParts = pronouns.Split('/'); ;
        playerPronounSubject = pronounParts[0];
        playerPronounObject = pronounParts[1];
    }
}
