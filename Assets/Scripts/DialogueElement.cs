﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DialogueElement", menuName = "Dialogue/DialogElement", order = 1)]
[System.Serializable]
public class DialogueElement : ScriptableObject
{
    public Sprite image;
    public string characterName;
    [TextArea(3, 10)]
    public string[] sentences;
    public DialogueElement nextDialogue, previousDialogue;
    public string audioName;
}
