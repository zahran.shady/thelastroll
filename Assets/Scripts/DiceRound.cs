﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

[CommandInfo("Dice",
             "Dice Round",
             "Plays a dice round and updates the results variables")]
public class DiceRound : Command
{
    int roundNumber;
    int player1Dice, player1Score, player2Dice, player2Score;
    float randomResult;
    public override void OnEnter()
    {
        GetVars();
        RollDice();
        UpdateVars();
        Continue();
    }

    public override string GetSummary()
    {
        return "Updates player dice results";

    }

    public override Color GetButtonColor()
    {
        return new Color32(235, 222, 52, 255);
    }

    public void RollDice()
    {
        switch (roundNumber)
        {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                //here is 50 50
                randomResult = Random.Range(0f, 100f);
                if (randomResult <= 50f)
                {
                    //player1 win
                    PlayerWinRound(1);
                }
                else
                {
                    //player1 lose
                    PlayerLoseRound(1);
                }
                break;
            case 6:
                //here winner wins this round
                if (player1Score > player2Score)
                {
                    //player1 win
                    PlayerWinRound(1);
                }
                else
                {
                    //player1 lose
                    PlayerLoseRound(1);
                }
                break;
            case 7:
                //here loser wins
                if (player1Score < player2Score)
                {
                    //player1 win
                    PlayerWinRound(3);
                }
                else
                {
                    //player1 lose
                    PlayerLoseRound(3);
                }
                break;
            case 8:
                //here loser wins
                if (player1Score < player2Score)
                {
                    //player1 win
                    PlayerWinRound(3);
                }
                else
                {
                    //player1 lose
                    PlayerLoseRound(3);
                }
                break;
            case 9:
                //here is 50 50
                randomResult = Random.Range(0f, 100f);
                if (randomResult <= 0.5f)
                {
                    //player1 win
                    PlayerWinRound(3);
                }
                else
                {
                    //player1 lose
                    PlayerLoseRound(3);
                }
                break;
            default:
                break;
        }
    }

    void PlayerWinRound(int scoreIncrement)
    {
        player1Dice = Random.Range(2, 7);
        player2Dice = Random.Range(1, player1Dice);
        player1Score += scoreIncrement;

    }

    void PlayerLoseRound(int scoreIncrement)
    {
        player2Dice = Random.Range(2, 7);
        player1Dice = Random.Range(1, player2Dice);
        player2Score += scoreIncrement;
    }

    void UpdateVars()
    {
        GetFlowchart().SetIntegerVariable("Player1Dice", player1Dice);
        GetFlowchart().SetIntegerVariable("Player2Dice", player2Dice);
    }

    void GetVars()
    {
        
        roundNumber = GetFlowchart().GetIntegerVariable("RoundNumber");
        player1Dice = GetFlowchart().GetIntegerVariable("Player1Dice");
        player2Dice = GetFlowchart().GetIntegerVariable("Player2Dice");
        player1Score = GetFlowchart().GetIntegerVariable("Player1Score");
        player2Score = GetFlowchart().GetIntegerVariable("Player2Score");
    }
}
