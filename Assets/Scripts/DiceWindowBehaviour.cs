﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceWindowBehaviour : MonoBehaviour
{
    public static DiceWindowBehaviour instance;
    public Image myImage;
    public Sprite[] diceImages;
    public List<DiceStruct> Dices;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void UpdateImage(int diceValue)
    {
        Sprite[] tempSpriteList = Dices[diceValue - 1].variations;
        int randValue = Random.Range(0, tempSpriteList.Length);
        myImage.sprite = tempSpriteList[randValue];
    }

    [System.Serializable]
    public struct DiceStruct
    {
        public Sprite[] variations;
    }
}
