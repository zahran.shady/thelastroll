﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Reselectables : MonoBehaviour
{
    public List<Button> menuButtons;

    private void Start()
    {
        //for (int i = 0; i < transform.childCount; i++)
        //{
        //    menuButtons.Add(transform.GetChild(i).GetComponent<Button>());
        //}
        menuButtons[0].GetComponent<Button>().Select();
    }

    private void Update()
    {
        if (Input.anyKeyDown && !IsAnyButtonSelected())
        {
            menuButtons[0].GetComponent<Button>().Select();
        }
    }

    bool IsAnyButtonSelected()
    {
        bool result = false;
        foreach (var button in menuButtons)
        {
            if (EventSystem.current.currentSelectedGameObject==button.gameObject)
            {
                result = true;
            }
        }
        return result;
    }
}
