﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sentences : MonoBehaviour
{
    [TextArea(3, 10)]
    public string[] sentences;
}
