﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ArcElement", menuName = "Dialogue/ArcElement", order = 1)]
public class ArcElement : ScriptableObject
{

    public DialogueElement[] dialogues;
    public AudioClip audio;
}
