﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Fungus;
public class DiceGameManager : MonoBehaviour
{

    public Slider chancesSlider;
    public TextMeshProUGUI player1Chance, player2Chance;
    public TextMeshProUGUI roundsText, player1ScoreText, player2ScoreText, player1Dice, player2Dice;
    private int player1Score, player2Score, rounds;
    private int player1TempValue;
    private int player2TempValue;

    public Flowchart myFlowchart;
    private int player1Result, player2Result;
    private void Awake()
    {
        player1Score = 0;
        player2Score = 0;
        rounds = 1;
    }
    public void UpdateChancesValues()
    {
        float tempValue = chancesSlider.value;
        player1Chance.SetText(tempValue.ToString());
        player2Chance.SetText((100f - tempValue).ToString());

    }

    public void RollDice()
    {
        
        //player must win
        if (chancesSlider.value == 100f)
        {
            PlayerWinRound();
        }
        //player must lose
        else if (chancesSlider.value == 0f)
        {
            PlayerLoseRound();
        }
        else
        {
            float tempRandomChance = Random.Range(0f, 100f);
            if (tempRandomChance<=chancesSlider.value)
            {
                //player win
                PlayerWinRound();
            }
            else
            {
                //player lose
                PlayerLoseRound();
            }
        }
        rounds++;
        roundsText.SetText("Round\n" + rounds.ToString());
    }

    public void ResetAllValues()
    {
        player1Score = 0;
        player2Score = 0;
        rounds = 1;
        chancesSlider.value = 50f;
        roundsText.SetText("Round\n1");
        player1ScoreText.SetText("Score\n0");
        player2ScoreText.SetText("Score\n0");
        player1Dice.SetText("Player1 Dice: 0");
        player2Dice.SetText("Player2 Dice: 0");

    }

    void PlayerWinRound()
    {
        player1TempValue = Random.Range(2, 7);
        player2TempValue = Random.Range(1, player1TempValue);
        player1Score++;


        player1Dice.text = "Player1 Dice: " + player1TempValue.ToString();
        player2Dice.text = "Player2 Dice: " + player2TempValue.ToString();
        player1ScoreText.SetText("Score\n" + player1Score.ToString());

        player2Result = player2TempValue;
        player1Result = player1TempValue;
    }

    void PlayerLoseRound()
    {
        player2TempValue = Random.Range(2, 7);
        player1TempValue = Random.Range(1, player2TempValue);
        player2Score++;

        player1Dice.text = "Player1 Dice: " + player1TempValue.ToString();
        player2Dice.text = "Player2 Dice: " + player2TempValue.ToString();
        player2ScoreText.SetText("Score\n" + player2Score.ToString());

        player2Result = player2TempValue;
        player1Result = player1TempValue;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
