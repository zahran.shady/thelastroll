﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public Sound[] sounds;

    private void Awake()
    {
        if (instance==null)
        {
            instance = this;
        }
        else if(instance!=this)
        {
            Destroy(gameObject);
        }
        foreach (var sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.playOnAwake = sound.playOnAwake;
            sound.source.loop = sound.loop;
            if (sound.source.playOnAwake)
            {
                sound.source.Play();
            }
        }
    }

    public void Play(string soundName)
    {
        Sound s = Array.Find(sounds, sound => sound.audioName == soundName);
        s.source.Play();
    }
}
