﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class MainMenu : MonoBehaviour
{
    public GameObject MainMenuUI;
    public GameObject NameSelectionUI;
    public GameObject PronounsSelectionUI;
    public static MainMenu instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    public void ShowMainMenu()
    {
        MainMenuUI.SetActive(true);
    }

    public void ShowNameSelection()
    {
        MainMenuUI.SetActive(false);
        NameSelectionUI.SetActive(true);
    }

    public void ShowPronounsSelection()
    {
        NameSelectionUI.SetActive(false);
        PronounsSelectionUI.SetActive(true);
    }
}
