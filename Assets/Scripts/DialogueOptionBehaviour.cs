﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DialogueOptionBehaviour : MonoBehaviour, ISelectHandler, IPointerEnterHandler
{
    public string SoundEffectName;
    public void OnSelect(BaseEventData eventData)
    {
        //SoundManager.instance.Play(SoundEffectName);
    }

    public void OnBtnClick()
    {
        if (EventSystem.current.currentSelectedGameObject == gameObject)
        {
            SoundManager.instance.Play(SoundEffectName);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (EventSystem.current.currentSelectedGameObject != gameObject)
        {
            gameObject.GetComponent<Button>().Select();
        }
    }
}
