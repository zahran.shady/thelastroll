﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


[CommandInfo("Dice",
             "Update Dice Window",
             "Updates the dice window UI with dice results")]
public class UpdateDiceWindow : Command
{
    public string diceVarName;
    int diceVal;
    public override void OnEnter()
    {
        GetVars();

        DiceWindowBehaviour.instance.UpdateImage(diceVal);

        Continue();
    }

    public override string GetSummary()
    {
        return "Updates dice window";

    }

    public override Color GetButtonColor()
    {
        return new Color32(235, 222, 52, 255);
    }



    void GetVars()
    {
        diceVal = GetFlowchart().GetIntegerVariable(diceVarName);
    }
}
