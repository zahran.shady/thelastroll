﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonElement : MonoBehaviour
{
    public Sprite currentSprite;
    public Sprite basicSprite, selectedSprite;

    private void Init()
    {
        currentSprite = basicSprite;
    }
}
