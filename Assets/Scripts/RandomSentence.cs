﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;


[CommandInfo("Utilities",
             "Random Sentence",
             "Picks a random sentence from a gameobject on the scene with the Sentences component, and assigns it to the designated string variable to be used")]
public class RandomSentence : Command
{
    public Sentences mySentences;
    public string VariableName;
    private string chosenSentence;
    public override void OnEnter()
    {
        GetRandomSentence();
        Continue();
    }

    public override string GetSummary()
    {
        return "Updates variable withe a random sentence";

    }

    public override Color GetButtonColor()
    {
        return new Color32(235, 222, 52, 255);
    }

    private void GetRandomSentence()
    {
        int rnd = Random.Range(0, mySentences.sentences.Length);
        chosenSentence = mySentences.sentences[rnd];
        GetFlowchart().SetStringVariable(VariableName, chosenSentence);
    }
}
